import React, { useState, useEffect } from 'react';
import Header from './components/header/Header';
import './App.css';
import Sidebar from './components/sidebar/Sidebar';
import Aside from './components/aside/Aside';
import Catalog from './components/catalog/Catalog';
import Modal from './components/Modal/Modal';
import axios from 'axios';
import {BrowserRouter as Router , Switch , Route } from 'react-router-dom';

const App = () => {
  const [products, setProducts] = useState([]);
  const [filteredProducts, setFilteredProducts] = useState([]);
  const [sortType, setSortType] = useState('');
  const [query, setQuery] = useState("");
  const [checkedProducts, setCheckedProducts] = useState([]);
  const [selectedProduct, setSelectedProduct] = useState(null);
  const [categories , setCategories] = useState();
 


  useEffect(() => {
    axios
        .get("https://fakestoreapi.com/products")
        .then(res => {
            localStorage.setItem('products',JSON.stringify(res.data));
            setProducts(res.data);
        })
  }, []);

   useEffect(()=> {
    const delay =setTimeout(()=>{
      let filteredArray = products.length? products:JSON.parse(localStorage.getItem('products'));
      filteredArray = filteredArray.filter((item)=> {
        return item.title.toLowerCase().includes(query.toLowerCase())
      });
      setFilteredProducts ([...filteredArray])
    }, 1000)
    return (()=> {
      clearTimeout(delay)
    })
  }, [query]);
  
  

  useEffect(() => {
    let filteredArray = filteredProducts.length ? filteredProducts : products;
    const sortArray = type => {
      const types = {
        sortby: 'Sort By: New Arrivals',
        asc: 'Price: High To Low',
        desc: 'Price: Low To High',
        profH: 'Profit: High To Low',
        profL: 'Profit: Low To High',
      };
      const sortProperty = types[type];
      if (sortProperty === "Sort By: New Arrivals") {
        filteredArray = filteredArray.sort((a, b) => (b.id > a.id) ? 1 : -1);
      } else if (sortProperty === "Price: High To Low") {
        filteredArray = filteredArray.sort((a, b) => b.price - a.price);
      } else if (sortProperty === "Price: Low To High") {
        filteredArray = filteredArray.sort((a, b) => a.price - b.price);
      } else if (sortProperty === "Profit: High To Low") {
        filteredArray = filteredArray.sort((a, b) => a.title.localeCompare(b.title));
      } else if (sortProperty === "Profit: Low To High") {
        filteredArray = filteredArray.sort((a, b) => b.title.localeCompare(a.title));
      }
      setFilteredProducts([...filteredArray])
      
    };

    sortArray(sortType);
  },
    [sortType, products, setProducts ]
  );
  
  

  const onChanged = (e) => setSortType(e.target.value);

  const changeQuery = (e) => {
    setQuery(e.target.value);
  }


  const checkboxChanged = (e, product) => {
    if (checkedProducts.includes(product)) {
      setCheckedProducts([...checkedProducts.filter(prod => prod.id !== product.id)])
    } else {
      setCheckedProducts([...checkedProducts, product])
    }
  }

  const selectAll = () => {
    setCheckedProducts([...products])
  }

  const clearSelectedProducts = () => {
    setCheckedProducts([])
  }


  const chooseProduct = (product) => {
    setSelectedProduct(product);
  }
  // categories 

    useEffect (()=>{
      // let filteredArray = filteredProducts.length ? filteredProducts : products;
      const filteredCategory= products.filter(item => 
          item.category === categories)
         setFilteredProducts ([...filteredCategory]
        )
        console.log(filteredCategory);
    },[categories]);



  return (
    <Router>
    <div className="App">
      {selectedProduct && <Modal selectedProduct={selectedProduct} setSelectedProduct={setSelectedProduct}/>}
      <Sidebar />
      <div className="god">
        <Aside setCategories={setCategories}/>
        <div className="Content">
          <Header selectAll={selectAll} clearSelectedProducts={clearSelectedProducts} query={query} onChange={onChanged} changeQuery={changeQuery}  checkedProductsCount={checkedProducts.length} numberOfProducts={products.length} />
          <div className="catalog">
            <Catalog categories={categories} filteredProducts={filteredProducts} chooseProduct={chooseProduct} products={products} checkboxChanged={checkboxChanged} checkedProducts={checkedProducts} />
          </div>
        </div>
      </div>
    </div>
    </Router>
  );
}

export default App;
