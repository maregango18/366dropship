import React from 'react';
import './Search.css';

const Search = ({ changeQuery, getFinalQuery, query }) => {

    return (
        <div classNameName={"search__content"}>
            <input
                id={"search"}
                className={"search__input"}
                type={"text"}
                placeholder={"Search"}
                value={query} onChange={changeQuery}
            />
            <button
                id={"search__button"}
                className={"header__searchbtn"}
                type={"submit"}
                onClick={query}
            >
                <i className="fas fa-search fa-rotate-90"></i>
            </button>
        </div>
    );
};
export default Search;


