import React from 'react';
import './Sort.css';

const Sort = ({ onChange }) => {


    return (
        <div className="sort__section">
            <i class="fas fa-sort-amount-up-alt" id="icon"></i>
            <select id="sort" onChange={onChange}>
                <option className="sortby" value="sortby">Sort By: New Arrivals </option>
                <option className="sort__value" value="asc">Price: High To Low</option>
                <option className="sort__value" value="desc">Price: Low To High</option>
                <option className="sort__value" value="profH">Profit: High To Low</option>
                <option className="sort__value" value="profL">Profit: Low To High</option>
            </select>
        </div>
    );
};

export default Sort;