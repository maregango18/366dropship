import React from 'react';

const Modal = ({selectedProduct,setSelectedProduct}) => { 
    return (
        <div className="modal__wrapper" onClick={() => setSelectedProduct(null)}>
          <div className="modal" onClick = {e => {e.stopPropagation();}} >
            <div className="modal__catalog__left">
              <div class="modal__catalog__prices"> 
                  <div>$100</div>
                  <div>${selectedProduct.price}</div>
                  <div>10%(10$)</div> 
              </div>
              <div class="modal__catalog__photo" >
                <img src={selectedProduct.image} alt=" " />
              </div>
            </div>
            <div className="modal__catalog__right">
              <div className="modal__close__btn" onClick={() => setSelectedProduct(null)}>x</div>
              <div className="info">
                <div>SKU# bgb-s0422395 COPY</div>
                <div className="supplier"> <span className="modal__supplier"> Suppliers:</span> SP-Supplier115</div>
              </div>
              <div className="modal__catalog__title"> {selectedProduct.title}</div>
              <div class="button">
                <button class="btn btn--modal">Add To My Inventory</button>
              </div>
              <div className="product__details">
                <div class="tabs">
                  <div class="tabs__item">Product Details</div>
                  <div class="tabs__item">Shipping Rates</div>
                </div>
                <p className="modal__catalog__description">{selectedProduct.description}</p>
              </div>
            </div>
          </div>
        </div>
    );
};

export default Modal;


