import React from 'react';
import './Catalog.css';

const Product = ({ product, checkboxChanged, checkedProducts, chooseProduct }) => {
    return (
        
        <div className={`catalog__product ${checkedProducts.includes(product) && 'catalog__product--selected'}`} >
            <input className="checkbox__btn" type="checkbox" onChange={e => checkboxChanged(e, product)} checked={checkedProducts.includes(product)}></input>
            <div class="catalog__photo" onClick={() => chooseProduct(product)}>
                <img src={product.image} alt=" " />
            </div>
            <div className="catalog__title"> {product.title}</div>
            <div className="supplier"><span className="by"> By: </span> SP-Supplier115</div>
            <div class="catalog__prices"> 
                <div className="cat__price">
                    <div>$100</div>  
                    <span>RRP</span>
                </div>
                <div className="cat__price">
                    <div>${product.price}</div>  
                    <span>COST</span>
                </div>
                <div className="cat__price">
                    <div className="profit">10%($10)</div> 
                    <span>PROFIT</span>
                </div>
                
            </div>
        </div>
    );
}

export default Product;