import { Container } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import './Catalog.css';
import Product from "./Product";
import Grid from '@material-ui/core/Grid';


const Catalog = ({allProducts ,categories, filteredProducts , products , checkboxChanged , checkedProducts , chooseProduct }) => {

    // const [products, setProducts] = useState([]);

    // useEffect(() => {
    //     getProducts();
    // }, []);

    // const getProducts = async () => {
    //     const response = await fetch(
    //         "https://fakestoreapi.com/products"
    //     );
    //     const data = await response.json();
    //     setProducts(data);
    //     console.log(data);
    // };
    
    
    return (
        <>
        <Container>
            <Grid container>
            {filteredProducts && filteredProducts.map(product => (
                <Grid item key={product.id} xs={12} md={6} lg={4}>
                <Product product={product} chooseProduct={chooseProduct} checkboxChanged={checkboxChanged} checkedProducts={checkedProducts}/>
                </Grid>
            ))}
            </Grid>
        </Container>
        </>
    );
};

export default Catalog;
